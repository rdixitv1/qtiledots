#+TITLE:
#+DATE:    September 6, 2021
#+SINCE:   <replace with next tagged release version>
#+STARTUP: inlineimages nofold

* Table of Contents :TOC_3:noexport:
- [[#files][Files]]
- [[#troubleshooting][Troubleshooting]]

* Files
1. ~config.el~ - Main configuration
2. ~config.org~ - Literate config; Code blocks from here get tangled to ~config.el~. Make sure you have =:config literate= enabled in ~init.el~.
3. ~init.el~ - Packages that Doom initializes with.
4. ~packages.el~ - Where you enable packages that are not in ~init.el~.
5. ~custom.el~ - Don't edit this file by hand; Autogenerated by Emacs
6. ~eshell/aliases~ - Aliases for Eshell
7. ~eshell/profile~ - Initialize Eshell
8. ~themes/~ - Personal Themes

* Troubleshooting
1. If you are using a distribution of Emacs that is not Doom, you may get errors.
2. Make sure that the package that is being mentioned in the code block is installed in ~packages.el~ or ~init.el~.
